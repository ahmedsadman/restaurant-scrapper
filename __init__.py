from flask import Flask, escape, request, render_template, url_for, redirect, session
import subprocess
import sys
from yelp import yelp_scrapper
from tripadvisor import tripadvisor_scrapper
from zomato import zomato_scrapper
from _google import google_scrapper
from flask import send_file

app = Flask(__name__, template_folder='static')


@app.route('/')
def index():
    return render_template("home.html")


@app.route('/process', methods=["POST"])
def hello():
    if request.method == "POST":
        if request.form["submit_button"] == 'yelp':
            result = request.form["yelp-search"]
            yelp_scrapper(result)
            file = "yelp_csv"
        elif request.form["submit_button"] == 'google':
            result = request.form["google-search"]
            google_scrapper(result)
            file = "google_map_csv"
        elif request.form["submit_button"] == 'tripadvisor':
            result = request.form["tripadvisor-search"]
            tripadvisor_scrapper(result)
            file = "trip_advisor_csv"
        elif request.form["submit_button"] == 'zomato':
            result = request.form["zomato-search"]
            print("resul======", result)
            zomato_scrapper(result)
            file = "zomato_csv"

        return redirect(url_for('success', download=file))


@app.route('/success')
def success():
    return render_template('download.html', download=request.args.get('download'))


@app.route('/google_map_csv/')
def download():
    path = "google_map.csv"
    return send_file(path, as_attachment=True)


@app.route('/yelp_csv/')
def download_yelp():
    path = "yelp.csv"
    return send_file(path, as_attachment=True)


@app.route('/trip_advisor_csv/')
def download_trip_advisor():
    path = "USA.csv"
    return send_file(path, as_attachment=True)


@app.route('/zomato_csv/')
def download_zomato():
    path = "zomato.csv"
    return send_file(path, as_attachment=True)


if __name__ == '__main__':
    app.debug = True
    app.run(debug=True)
